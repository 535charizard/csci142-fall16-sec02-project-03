package mvc;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.lang.reflect.Method;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicOptionPaneUI.ButtonActionListener;

public class View  
{
 /**
     *This class displays all of the buttons, JLabels and JTextArea's to the GUI.
     *THis is where the GUI is constructed which itself never relates to the
     *model or Puzzle.
     * 
     * @ author DeArvis Troutman 
	 * @ author DeArvis Troutman
	 * 
	 * September 28th at Midnight
	 * 
	 *The information required to run this class is frame,wrd1,
	 *wrd2,resultWord,solution,word1Label,word2Label,resultWordLabel,solutionWord1,
	 *solutionWord2,additionLine,solutionResultWord,missionImpossible,clear,solve,
	 *myClearListener,mySolveListener,wordPanel,buttonPane,solutionPane,grid
	 *,flowlayout,flowLayoutForSolution,and answers.	
	 */
	private JFrame frame = new JFrame ("CryptArithmetic GUI");
	private JTextField wrd1 = new JTextField("Please use all Caps...");
	private JTextField wrd2 = new JTextField("Please use all Caps...");
	private JTextField resultWord = new JTextField("please use all Caps...");
	private JLabel solution = new JLabel("Solution: ");
	private JLabel word1Label = new JLabel("Enter Word 1 Below:");
	private JLabel word2Label = new JLabel("Enter Word 2 Below:");
	private JLabel resultWordLabel = new JLabel("Enter Result Word Below:");
	private JLabel solutionWord1 = new JLabel(" ");
	private JLabel solutionWord2 = new JLabel(" ");
	private JLabel additionLine = new JLabel(" ");
	private JLabel solutionResultWord = new JLabel(" ");
	private JLabel missionImpossible;
	private JButton clear = new JButton("Clear");
	private JButton solve = new JButton("Solve");
    private ButtonListener myClearListener;
	private ButtonListener mySolveListener;
	private JPanel wordPanel = new JPanel();
	private JPanel buttonPane = new JPanel();
	private JPanel solutionPane = new JPanel();
	private LayoutManager grid = new BoxLayout(wordPanel,BoxLayout.Y_AXIS);
	private LayoutManager flowLayout = new FlowLayout();
	private LayoutManager flowLayoutForSolution = new FlowLayout();
	private LayoutManager answers = new BoxLayout(solutionPane,BoxLayout.Y_AXIS);
	
	/**
	 * This method takes in the controller and updates the GUI with components
	 * connecting itself with the controller which links the view and model.
	 * 
	 * @param controller
	 * 
	 * @DeARvis Troutman
	 */
	public View(Controller controller)
	{
	    frame.setSize(470, 350);
		frame.setLayout(new BorderLayout());
		associateListeners(controller);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
       
		
	    wordPanel.setLayout(grid);
	    buttonPane.setLayout(flowLayout);
        solutionPane.setLayout(answers);
	
        wordPanel.add(word1Label);
        wordPanel.add(getWrd1());
        wordPanel.add(Box.createRigidArea(new Dimension(52,70)));
        
        wordPanel.add(word2Label);
	    wordPanel.add(getWrd2());
	    wordPanel.add(Box.createRigidArea(new Dimension(52,70)));
	    
	    wordPanel.add(resultWordLabel);
	    wordPanel.add(getResultWord());
	    wordPanel.add(Box.createRigidArea(new Dimension(52,70)));
	   
	    solutionPane.add(solution);
	    solutionPane.add(Box.createRigidArea(new Dimension(52,10)));
	    solutionPane.add(getSolutionWord1());
	    solutionPane.add(Box.createRigidArea(new Dimension(52,10)));
	    solutionPane.add(solutionWord2);
	    solutionPane.add(Box.createRigidArea(new Dimension(52,10)));
	    solutionPane.add(additionLine);
	    solutionPane.add(Box.createRigidArea(new Dimension(52,10)));
	    solutionPane.add(solutionResultWord);
	    
	    // Creating MouseListeners for the clear and save button
	    solve.addMouseListener(mySolveListener);
	    clear.addMouseListener(myClearListener);
	    
	    buttonPane.add(solve);	
	    buttonPane.add(clear);

    	//frame.setResizable(false);
	    frame.add(solutionPane,BorderLayout.EAST);
	    frame.add(wordPanel,BorderLayout.WEST);
	    frame.add(buttonPane,BorderLayout.SOUTH);
	    frame.setVisible(true);
	}
	/**
	 * This method links the JButtons with the ButtonListener and mouseListener.
	 * It it correlates the designated buuton with the designated method in the 
	 * controller class. 
	 * 
	 * @param controller
	 * 
	 * @DeArvis Troutman
	 */
	public void associateListeners(Controller controller)
	{
        Class<? extends Controller> controllerClass;
        Method clearMethod,
               solveMethod;
      
        controllerClass = controller.getClass();
        
        clearMethod = null; 
        solveMethod = null;
     
   
        // Associate method names with actual methods to be invoked
        try
        {
           clearMethod = controllerClass.getMethod("clear",(Class<?>[])null);
           solveMethod = controllerClass.getMethod("solve",(Class<?>[])null);        
        }
        catch(NoSuchMethodException exception)
        {
           String error;
           error = exception.toString();
           System.out.println(error);
         
        }
        catch(SecurityException exception)
        {
           String error;
           error = exception.toString();
           System.out.println(error);
        }
        
        // Set up listeners with actual argument values passed in
        // to methods
        
        mySolveListener 
               = new ButtonListener(controller, solveMethod,null);
        myClearListener 
               = new ButtonListener(controller, clearMethod,null);
		
	}
	public JTextField getWrd1()
	{
		return wrd1;
	}

	public void setWrd1(JTextField wrd1) 
	{
		this.wrd1 = wrd1;
	}

	public JTextField getWrd2()
	{
		return wrd2;
	}

	public void setWrd2(JTextField wrd2) 
	{
		this.wrd2 = wrd2;
	}

	public JTextField getResultWord() 
	{
		return resultWord;
	}

	public void setResultWord(JTextField resultWord)
	{
		this.resultWord = resultWord;
	}

	public JLabel getSolutionWord1()
	{
		return solutionWord1;
	}

	public void setSolutionWord1(JLabel solutionWord1)
	{
		this.solutionWord1 = solutionWord1;
	}
	
	
	
	public JLabel getSolutionWord2()
	{
		return solutionWord2;
	}

	public void setSolutionWord2(JLabel solutionWord2) 
	{
		this.solutionWord2 = solutionWord2;
	}



	public JLabel getSolutionResultWord()
	{
		return solutionResultWord;
	}

	public void setSolutionResultWord(JLabel solutionResultWord)
	{
		this.solutionResultWord = solutionResultWord;
	}

	public JLabel getadditionLine() 
	{
	    return additionLine;
	}

	public void setAdditionLine(JLabel additionLine)
	{
		this.additionLine = additionLine;
	}

}

package mvc;

public class Puzzle 
{

    /** 
	* The Purpose of this class is to implement methods which allows the user
	* to be able to find the cryptoarithmetic answer to all cryptoarithmetic
	* problems.
	* 
	* @ author DeArvis Troutman 
	* @ author DeArvis Troutman
	* 
	* November 9th at Midnight
	* 
	*The information required to run this class is letters,assigned,value,myWord1,
	*myWord2,myResultWord,myIsPuzzledSolved and addingSum
	*
	*/
	private char letters[] = new char [26]; 
	
	private int assigned[] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
                             ,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
                             ,-1,-1,-1,-1};
	private int value = 0;
	private String myWord1 = " ";
	private String myWord2 = " ";
	private String myResultWord = " ";
	private boolean myIsPuzzlSolved;
    private int addingSum =0;
	/**
	 * This method allows the user to store the maxlenthgh, as well as
	 * the three words they are using in their calculation.
	 * 
	 * @param maxLength
	 * @param word1
	 * @param word2
	 * @param result
	 * 
	 * @DeArvis Troutman
	 */
	//stores all the data stored on the JTextArea's
	public Puzzle(int maxLength,String word1,String word2, String result)
	{
	    myWord1 = word1;
	    myWord2 = word2;
	    myResultWord = result;
	  
	   
	    int word1NumOfLetters = myWord1.length();
	    int word2NumOfLetters = myWord2.length();
	   
	    //myMaxLength = word1NumOfLetters + word2NumOfLetters;
	    addingSum =  word1NumOfLetters + word2NumOfLetters;
}
	/**
	 * This method is suppose to check the method checkPuzzleResult
	 * and validate if it is true or not.
	 * 
	 * @return
	 * 
	 * @DeArvis Troutman
	 */
	public boolean checkIfSolved()
	{
	   if(solvePuzzle() == true)
	   {
		   return true;
	   }
		   
		return false;
	}
	/**
	 * This method assigns a specified character to a specified digit.
	 * 
	 * @param digit
	 * @param character
	 * 
	 * @DeArvis Troutman
	 */
	public void assignDigit(int digit, char character)
	{
		//Make a for loop for the two arrays assigned and letters
		// so that easily accessible.
		for(int p = 0; p < letters.length;p++)
		{
		    if(character == letters[p])
		    {
		        assigned[p] = digit;
		    }
		}
	}
	/**
	 * This digit unassigns a digit to a letter if the letter was previously
	 * assigned to a digit. 
	 * 
	 * @param digit
	 * @param letter
	 * 
	 * @DeArvis Troutman
	 */
	public void unAssignDigit(int digit, char letter)
	{
		//Checks array to see if letter exist
		for(int i = 0; i < letters.length;i++)
		{
			if(letter == letters[i])
			{
				System.out.println("Found the Letter");
				assigned[i] = -1;
			}
		}
	}
	/**
	 * This method checks if a specified digit has been assigned to any letter
	 * in the letters array.
	 * 
	 * @param digit
	 * @return
	 * 
	 * @DeArvis Troutman
	 */
	public boolean checkIfDigitUsed(int digit)
	{
		//While going through the recursion, was the digit used.
		return true;
	}
/**
     * This method checks every single character in word 1, 
     * word 2 and the result word.
	 * 
	 * 
	 * @DeArvis Troutman
 */
	public void determineListOfLetters()
	{
		int numOfLetters = myWord1.length();
	
		for(int j = 0; j < myWord1.length();j++)
	    {
		   
			letters[j] = myWord1.charAt(j);
	    }
		
		for(int k = 0; k < myWord2.length();k++)
		{
			letters[k + myWord1.length()] = myWord2.charAt(k);
			
		}
		
		for(int l = 0; l < myResultWord.length();l++)
		{
			letters[l + addingSum] = myResultWord.charAt(l);
		}
		
		
		for(int le = 0; le < letters.length;le++)
		{
			System.out.println("Determine List of Letters: " + letters[le]);
		}
	}

	/**
	 * This method goes through the letters and assigned arrays and checks if
	 * every letter in the letters array has been assigned to a digit.
	 * 
	 * @return
	 * 
	 * @DeArvis Troutman
	 */
	//Checks if all leters that are used is assigned
	public Boolean checkIfAllLettersAssigned()
	{
	    //Make a for loop for the two arrays assigned and letters
		// so that they are easily accessible and check if assigned
		for(int i = 0; i < letters.length;i++)
	    {
		    for(int n = 0; n < assigned.length;n++)
	    	{
		    	if(assigned[n] != -1 )
                {
		   	        System.out.println("letter: " + letters[n]);
		    		return true;
				}
		    }
	    }
		
		return false;
	}
	/**
	 * This method is suppose to return true if the puzzle has been solved or
	 * false if the puzzle has not been solved.
	 * 
	 * @return
	 * 
	 * @DeArvis Troutman
	 */
	public boolean solvePuzzleResult()
	{
		//boolean result = this.solvePuzzle(myListOfLetters);
		//System.out.println("My Results are: " + result);
	    return myIsPuzzlSolved;
	}
	
	
	/**
	 * This method is supposed to hold the recursive algorithm used to solve any crypto
	 * algorrithmic problem.
	 * 
	 * @return
	 * 
	 * @DeArvis Troutman
	 */
	public boolean solvePuzzle()
	{
		determineListOfLetters(); // This picks out the letters in word 1 and word 2
								  // and assigns them as 0, then picks out chars in 
								  // my Result Word and assign each char to 13
		
		assignDigit(0,'H');
		assignDigit(0,'I');
		assignDigit(0,'P');
		assignDigit(0,'O');
		
		add();                    // This adds all of the assigned digits 
		                          // for word 1 and word 2
		       
			//Algorithm attempt #1
		if (checkIfAllLettersAssigned() == true)
		{
		    if(solvePuzzleResult() == true)
		    {
		    	return true;
		    	
		    }
		   
		    else if(solvePuzzleResult() == false)
		    {
		    	return false;
		    }
		    
		}	
		if(checkIfAllLettersAssigned() == false)
		{
			
		}
	
//		 If all characters from list already assigned
//       Return true if puzzle solved, false if not
//                    End if 
//		 For all remaining unassigned digits
//		 Assign digit to next unassigned letter
//		 If assignment possible
//		 Recursively call this Solver method with remaining letters
//		 If recursion successful
//		 Return true
//		 Else
//		 Unassign letter from digit
//		         End if
//		         End if
//		 End for
//		 Return false
		
		
		        //Algorith,recursive attempt #2
//     public int Solve(char letter)
//		for(int n =0; n < 10; n++)
//		   {
//			   add();
//			   if()
//			   if(n == 10)
//			   {
//				   Solve(n - 1);
//			   }
//		   }
		

                 // Algorithm attempt #3		
//		   for(int H =0; H < 10;H++)
//		   {
//			   for(int I =0; I < 10;I++)
//			   {
//				   for(int P =0; P < 10;P++)
//				   {
//					   for(int O =0; O < 10;O++)
//					   {
//						   
//					   }
//				   }
//			   }
				   
		return true;
}
	
	
	/**
	 * This method adds all of the assigned letters for word1 and word2
	 * 
	 * @return
	 * 
	 * @DeArvis Troutman
	 */
	public int add()
	{
	    
		for(int fl = 0; fl < assigned.length; fl++)
		{ 
		    if(assigned[fl] != -1)
		    {
			    value = value + assigned[fl];
			    System.out.println("Letter value: " + letters[fl]);
			    System.out.println("Assigned value: " + assigned[fl]);
		        System.out.println("Sum is: " + value);
		        System.out.println("");
		    }
		    
			//System.out.println(letters[fl] + "," + assigned[fl]);
		}
		return value;
	}
	public char[] getLetters()
	{
		return letters;
	}
	public void setLetters(char[] letters)
	{
		this.letters = letters;
	}
	public int[] getAssigned()
	{
		return assigned;
	}
	public void setAssigned(int[] assigned)
	{
		this.assigned = assigned;
	}
}

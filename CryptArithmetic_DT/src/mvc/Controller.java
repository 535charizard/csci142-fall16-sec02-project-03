package mvc;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class Controller {
	/**
	 * This class is connected to the model and the view,
	 *  linking the two with each other and notifying the 
	 *  other when an action or method is implemented.
	 *  
	 * @ author DeArvis Troutman 
	 * @ author DeArvis Troutman
	 * 
	 * September 28th at Midnight
	 * 
	 *The information required to run this class is int i, String s, String r,
	 *String e, View myView, and Puzzle myModel.
	 */
	
	 ///////////////////
     //  Properties   //
     ///////////////////
     private int i;
     private String s = " ";
     private String r = " ";
     private String e = " ";
     private View myView;
     private Puzzle myModel;
    
     ///////////////////
     //    Methods    //
     ///////////////////
    
     /**
     * Controller constructor; view must be passed in since 
     * controller has responsibility to notify view when 
     * some event takes place.
     * 
     * @DeArvis Troutman
     */
    public Controller()
    {
        myModel = new Puzzle(i,s,r,e);
        myView = new View(this);
    }
    
    /**
     * This method clears all information in the three JText Area's
     * 
     * @DeArvis Troutman
     */
    public void clear()
    {
        myView.getWrd1().setText("Cleared...");
    	myView.getWrd2().setText("Cleared...");
    	myView.getResultWord().setText("Cleared...");
   
    	
        myView.getSolutionWord1().setText("");
        myView.getSolutionWord2().setText("");
        myView.getadditionLine().setText("");
        myView.getSolutionResultWord().setText("");
    	
    	System.out.println("Clear");
    }
    /**
     * This method displays the solution on a JLabel on the leftside
     * of the GUI.
     * 
     * @DeArvis Troutman
     */
     public void solve()
     {
         s = myView.getWrd1().getText();
         r = myView.getWrd2().getText();;
         e = myView.getResultWord().getText();;
        
         
       
    	 //myModel.determineListOfLetters();	
         myView.getSolutionWord1().setText("  " + s);
         myView.getSolutionWord2().setText("+ " + r);
         myView.getadditionLine().setText("----------------------------");
         myView.getSolutionResultWord().setText(e);
          
         System.out.println("Solved");
    	
     }
}
